# Intex SPA

 
Команды для развертывания проекта;
   
    cp .env.example .env
    cp laradoc
    cp env.example .env
    ./build.sh
    ./start.sh
    ./run workspace
    composer install
    php artisan migrate --seed
    chmod 777 -R storage/framework/
    chmod 777 -R storage/logs/
    
Лучше конечно докер держать  в отдельной репозитории, но так быстро сделал. 