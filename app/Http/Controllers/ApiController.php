<?php

namespace App\Http\Controllers;

use App\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    public function getCategories()
    {
        $categories = $select = DB::table('categories')
            ->select('id', 'name')
            ->get();
        $data = [];

        foreach ($categories->toArray() as $item) {
            $data[] = ['id' => $item->id, 'name' => $item->name];
        }
        return response()->json(['data' => $data, 'success' => true]);
    }

    public function getPlaces()
    {
        $places = $select = DB::table('places')
            ->select('places.lat', 'places.lng', 'places.description', 'places.created_at as date', 'categories.name as category')
            ->join('categories', 'places.category_id', '=', 'categories.id')
            ->get();
        return response()->json(['data' => $places->toArray() , 'success' => true]);
    }

    public function createPlace(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'lat' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
                'lng' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
                'category' => 'required|int',
                'description' => 'max:255',
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'errors' => $validator->errors()]);
            }

            Place::firstOrCreate(
                [
                    'lat' => $request->get('lat'),
                    'lng' => $request->get('lng'),
                    'category_id' => $request->get('category'),
                    'description' => $request->get('description'),
                ]
            );
            return response()->json(['success' => [$request->all()]]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => ['full' => $e->getMessage()]]);
        }

    }
}
