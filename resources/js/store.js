import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        markers: [],
        googleApiKey: '',
    },
    getters: {

    },
    mutations: {
        setMarkers(state, markers) {
            state.markers = markers;
        },
        addMarker(state, marker) {
            state.markers.push(marker);
        },
    },
    actions: {}
});