import Vue from 'vue'
import App from './App.vue'
import store from './store.js'
import Router from './routes.js'

import VueResource from 'vue-resource'

Vue.use(VueResource);

new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router: Router
})
